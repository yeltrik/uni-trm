<?php

namespace Yeltrik\UniTrm\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Yeltrik\UniTrm\app\models\Year;

class YearController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Year::class);
    }

    /**
     * @param Year $year
     * @throws AuthorizationException
     */
    public function show(Year $year)
    {
        $this->authorize('view', $year);
    }

}
