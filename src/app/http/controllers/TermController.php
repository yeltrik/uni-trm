<?php

namespace Yeltrik\UniTrm\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Yeltrik\UniTrm\app\models\Term;
use Yeltrik\UniTrm\app\models\Year;

class TermController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param Year $year
     * @throws AuthorizationException
     */
    public function index(Year $year)
    {
        $this->authorize('viewAny', Term::class);
    }

    /**
     * @param Year $year
     * @param Term $term
     * @throws AuthorizationException
     */
    public function show(Year $year, Term $term)
    {
        $terms = $year->terms();

        $this->authorize('view', $term);
    }

}
