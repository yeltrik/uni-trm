<?php

namespace Yeltrik\UniTrm\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\UniTrm\app\models\Year;

class YearPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        return TRUE;
    }

    public function view(User $user, Year $year)
    {
        return TRUE;
    }
}
