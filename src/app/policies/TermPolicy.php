<?php

namespace Yeltrik\UniTrm\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\UniOrg\app\models\College;

class TermPolicy
{
    use HandlesAuthorization;


    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        return TRUE;
    }

    public function view(User $user, College $college)
    {
        return TRUE;
    }

}
