<?php

namespace Yeltrik\UniTrm\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\UniTrm\database\factories\YearFactory;

/**
 * Class Year
 *
 * @property int id
 * @property string name
 * @property string abbr
 *
 * @package Yeltrik\UniTrm\app\models
 */
class Year extends Model
{
    use HasFactory;

    protected $connection = 'uni_trm';
    public $table = 'years';

    /**
     * @return YearFactory
     */
    public static function newFactory()
    {
        return new YearFactory();
    }

    /**
     * @return HasMany
     */
    public function terms()
    {
        return $this->hasMany(Term::class);
    }

}
