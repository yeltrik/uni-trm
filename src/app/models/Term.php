<?php

namespace Yeltrik\UniTrm\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniTrm\database\factories\TermFactory;

/**
 * Class Term
 *
 * @property int id
 * @property int year_id
 * @property string name
 * @property string abbr
 *
 * @property string fullAbbreviation
 *
 * @property Year year
 *
 * @package Yeltrik\UniTrm\app\models
 */
class Term extends Model
{
    use HasFactory;

    public static $termAbbrNames = [
        'WI' => 'Winter',
        'SP' => 'Spring',
        'SU' => 'Summer',
        'FA' => 'Fall',
        //'OD' => 'On Demand',
    ];

    protected $connection = 'uni_trm';
    public $table = 'terms';

    /**
     *
     */
    public function getFullAbbreviationAttribute()
    {
        $parts = [];
        $parts[] = $this->year->abbr;
        $parts[] = $this->abbr;
        return implode (" ", $parts);
    }

    public static function newFactory()
    {
        return new TermFactory();
    }

    public function year()
    {
        return $this->belongsTo(Year::class);
    }

}
