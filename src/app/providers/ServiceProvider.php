<?php

namespace Yeltrik\UniTrm\app\providers;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    CONST CONFIG_DATABASE_CONNECTION_FILE_NAME = 'yeltrik-uni-trm-database-connections.php';
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (file_exists(config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME))) {
            // User Customizable
            $this->mergeConfigFrom(
                config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME), 'database.connections'
            );
            //dd(config('database'));
        } elseif (file_exists(static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME)) {
            // Non Customizable
            $this->mergeConfigFrom(
                static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME, 'database.connections'
            );
            //dd(config('database'));
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(static::packageSrc() . 'database/migrations');
        $this->loadRoutesFrom(static::packageSrc() . 'routes/web.php');
        $this->loadViewsFrom(static::packageSrc() . 'resources/views', 'uniOrg');

        if ($this->app->runningInConsole()) {
            $this->publishResources();
        }
    }

    protected static function packageSrc()
    {
        return __DIR__ . '/../../';
    }

    protected function publishResources()
    {
        // User Customizable
        $this->publishes([
            static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME => config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME),
        ], 'config');

        $this->publishes([], 'uniOrg');
    }

}
