<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniTrm\app\http\controllers\TermController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/year/{year}/term',
    [TermController::class, 'index'])
    ->name('years.terms.index');

Route::get('/year/{year}/term/{term}',
    [TermController::class, 'show'])
    ->name('years.terms.show');
