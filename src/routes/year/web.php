<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniTrm\app\http\controllers\YearController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/year',
    [YearController::class, 'index'])
    ->name('years.index');

Route::get('/year/{year}',
    [YearController::class, 'show'])
    ->name('years.show');

require 'term/web.php';
