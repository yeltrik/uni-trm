<?php

namespace Yeltrik\UniTrm\tests\feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\UniTrm\app\models\Year;

class YearTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('years.index'));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $year = Year::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('years.show', [$year]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }
}
