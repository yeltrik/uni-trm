<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\UniTrm\app\models\Term;
use Yeltrik\UniTrm\app\models\Year;

class TermTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $year = Year::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('years.terms.index', [$year]));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $term = Term::query()->inRandomOrder()->firstOrFail();
        $year = $term->year;
        $response = $this->actingAs($user, 'web')
            ->get(route('years.terms.show', [$year, $term]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }
}
