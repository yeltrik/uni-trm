<?php

namespace Yeltrik\UniTrm\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniTrm\app\models\Term;
use Yeltrik\UniTrm\app\models\Year;

class TermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year = Year::query()->where('name', '=', date('Y'))->firstOrCreate([
            'name' => date('Y'),
            'abbr' => date('y')
        ]);
        foreach ( Term::$termAbbrNames as $abbr => $name ) {
            $term = new Term();
            $term->year()->associate($year);
            $term->name = $name;
            $term->abbr = $abbr;
            $term->save();
        }
    }
}
