<?php

namespace Yeltrik\UniTrm\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniTrm\app\models\Year;

class YearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colleges = Year::factory()
            ->count(5)
            ->create();
    }
}
