<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uni_trm')->create('years', function (Blueprint $table) {
            $table->id();
            $table->integer('name');
            $table->integer('abbr');
            $table->timestamps();

            $table->unique(['name', 'abbr']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uni_trm')->dropIfExists('years');
    }
}
