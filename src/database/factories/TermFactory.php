<?php

namespace Yeltrik\UniTrm\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeltrik\UniTrm\app\models\Term;

class TermFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Term::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'abbr' => $this->faker->randomLetter
        ];
    }
}
