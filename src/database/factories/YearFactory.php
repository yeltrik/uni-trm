<?php

namespace Yeltrik\UniTrm\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeltrik\UniTrm\app\models\Year;

class YearFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Year::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $year = $this->faker->numberBetween(2015, 2020);
        $abbr = $year - 2000;
        return [
            'name' => $year,
            'abbr' => $abbr
        ];
    }
}
